<?php

require __DIR__ . '/app/bootstrap.php';
$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

// CONST
$configKeys = [
    'web/unsecure/base_url' => '',
    'web/secure/base_url' => '',
];

// FUNCTION
function read_options_from_args($args) {
    $result = [];

    for($i = 1; $i < count($args); $i++) {
        if(trim($args[$i]) == '') {
            continue;
        }

        $argParts = explode('=', $args[$i]);
        $option = str_replace('--', '', $argParts[0]);
        $value = trim($argParts[1], '"');
        $result[$option] = $value;
    }

    return $result;
}

function update_base_urls($baseUrl) {
    global $objectManager, $configKeys;

    if($baseUrl == 'undefined'
    || empty($baseUrl)) {
        return;
    }

    $resource = $objectManager->get(Magento\Framework\App\ResourceConnection::class);
    $writeConnection = $resource->getConnection();

    foreach($configKeys as $configKey => $suffix) {
        $query = 'UPDATE '.$writeConnection->getTableName('core_config_data').' SET value="'.$baseUrl.'" WHERE path="'.$configKey.'"';
        $writeConnection->query($query);
    }

    $cache = $objectManager->get(\Magento\Framework\App\CacheInterface::class);
    $cache->clean(['CONFIG']);
}

// MAIN
$options = read_options_from_args($argv);

if(isset($options['base_url'])) {
    update_base_urls($options['base_url'].'/');
}