<?php

if (empty($_SERVER['skip_xhprof_profiling']) 
&& empty($_GET['skip_xhprof_profiling'])
&& extension_loaded('xhprof')) {
    $profiler_namespace = 'magento';
    $xhprof_data = xhprof_disable();
    $xhprof_runs = new XHProfRuns_Default();
    $xhprof_runs->save_run($xhprof_data, $profiler_namespace);
}
