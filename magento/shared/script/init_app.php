<?php

require 'app/Mage.php';
Mage::app('admin');

// CONST
$currentScriptName = trim(str_replace(__DIR__, '', __FILE__), '/');
$baseUrlScriptName = 'index.php';

$configKeys = [
    'web/unsecure/base_url' => '',
    'web/secure/base_url' => '',
];

// FUNCTION

function read_options_from_args($args) {
    $result = [];

    for($i = 1; $i < count($args); $i++) {
        if(trim($args[$i]) == '') {
            continue;
        }
        
        $argParts = explode('=', $args[$i]);
        $option = str_replace('--', '', $argParts[0]);
        $value = trim($argParts[1], '"');
        $result[$option] = $value;
    }

    return $result;
}

function update_base_urls($baseUrl) {
    global $currentScriptName, $baseUrlScriptName, $configKeys;

    if($baseUrl == 'undefined' 
    || !Mage::isInstalled()
    || empty($baseUrl)) {
        return;
    }

    $resource = Mage::getSingleton('core/resource');
    $writeConnection = $resource->getConnection('core_write');

    foreach($configKeys as $configKey => $suffix) {
        $query = 'UPDATE '.$resource->getTableName('core_config_data').' SET value="'.$baseUrl.'" WHERE path="'.$configKey.'"';
        $writeConnection->query($query);
    }

    Mage::app()->cleanCache();
}

// MAIN
$options = read_options_from_args($argv);

if(isset($options['base_url'])) {
    update_base_urls($options['base_url'].'/');
}


