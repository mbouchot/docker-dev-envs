NGROK_URL=$(curl -s http://localhost:4040/api/tunnels|jq -r '.tunnels[]|select((.config.addr|test("127.0.0.1:'$1'")) and .proto=="http")|.public_url') \
APP_PORT=$1 \
POSTGRES_PORT=$2 \
ADMINER_PORT=$3 \
CURRENT_UID=$(id -u) \
CURRENT_GID=$(id -g) \
envsubst < docker-compose.yml.template > docker-compose.yml
