FROM php:5.6-apache

RUN a2enmod rewrite

RUN apt-get update \
    && apt-get install libpng-dev libfreetype6-dev libjpeg62-turbo-dev -qy \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

RUN apt-get update && apt-get install -y \
libmcrypt-dev \
libxml2-dev \
libcurl4-openssl-dev \
libicu-dev \
&& docker-php-ext-install -j$(nproc) mcrypt \
&& docker-php-ext-install pdo_mysql curl zip intl soap \
&& docker-php-ext-enable mcrypt pdo_mysql curl zip intl soap

RUN apt-get update && apt-get install -y \
git

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ARG CURRENT_UID
ARG CURRENT_GID
RUN usermod -u $CURRENT_UID www-data
RUN groupmod -g $CURRENT_GID www-data

RUN chmod 777 -R /var/www/html

RUN apt-get update && apt-get -y install unzip graphviz

RUN curl -L -o xhprof.zip https://github.com/RustJason/xhprof/archive/master.zip \
    && unzip xhprof.zip \
    && rm xhprof.zip \
    && mv xhprof-master xhprof \
    && mv xhprof /opt/ \
    && cd /opt/xhprof/extension \
    && phpize \
    && ./configure --enable-xhprof \
    && make \
    && make install

RUN docker-php-ext-enable xhprof

RUN mkdir /var/tmp/xhprof && chmod 777 -R /var/tmp/xhprof && rm -rf /var/tmp/xhprof/*
RUN ln -s /opt/xhprof/xhprof_html /var/www/html/xhprof

RUN apt-get update && apt-get install -q -y ssmtp mailutils
RUN echo "mailhub=maildev" >> /etc/ssmtp/ssmtp.conf

RUN apt-get update && apt-get -y install cron

ENV PORT 80
ENTRYPOINT []
CMD sed -i "s/80/$APP_PORT/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground
