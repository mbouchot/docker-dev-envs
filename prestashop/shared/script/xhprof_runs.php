<?php

if(!extension_loaded('xhprof')) {
    echo 'Xhprof is not loaded!!';
    die;
}

$_SERVER['skip_xhprof_profiling'] = true;
date_default_timezone_set('UTC');

$dir = '/var/tmp/xhprof';
$suffix = 'xhprof';

echo "<hr/>Existing runs:\n<ul>\n";
$files = glob("{$dir}/*.{$suffix}");
usort($files, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));
foreach ($files as $file) {
    list($run, $source) = explode('.', basename($file));
    echo '<li><a href="http://' . $_SERVER['HTTP_HOST'] . '/xhprof/index.php'
        . '?skip_xhprof_profiling=1&run=' . htmlentities($run) . '&source='
        . htmlentities($source) . '" target="_blank">'
        . htmlentities(basename($file)) . "</a><small> "
        . date("Y-m-d H:i:s", filemtime($file)) . "</small></li>\n";
}
echo "</ul>\n";
