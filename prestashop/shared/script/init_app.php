<?php

require(dirname(__FILE__).'/config/config.inc.php');

// CONST

// FUNCTION

function read_options_from_args($args) {
    $result = [];

    for($i = 1; $i < count($args); $i++) {
        if(trim($args[$i]) == '') {
            continue;
        }

        $argParts = explode('=', $args[$i]);
        $option = str_replace('--', '', $argParts[0]);
        $value = trim($argParts[1], '"');
        $result[$option] = $value;
    }

    return $result;
}

function update_base_urls($baseUrl) {
    if($baseUrl == 'undefined'
    || empty($baseUrl)) {
        return;
    }

    $domain = parse_url($baseUrl, PHP_URL_HOST);
    Db::getInstance()->execute('
        UPDATE `'._DB_PREFIX_.'shop_url` SET `domain`=\''.$domain.'\', `domain_ssl`=\''.$domain.'\'
    ');
}

// MAIN
$options = read_options_from_args($argv);

if(isset($options['base_url'])) {
    update_base_urls($options['base_url']);
}
