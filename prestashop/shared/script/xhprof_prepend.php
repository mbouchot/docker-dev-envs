<?php

if (extension_loaded('xhprof')) {
    include_once '/opt/xhprof/xhprof_lib/utils/xhprof_lib.php';
    include_once '/opt/xhprof/xhprof_lib/utils/xhprof_runs.php';
    xhprof_enable(XHPROF_FLAGS_NO_BUILTINS | XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
}
