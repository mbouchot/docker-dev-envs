# Docker-Dev-Plugin
## Synopsis

This project is a docker compose execution environment for e-Commerce applications coupled with Oyst and is focused to be used as development environments for Oyst team members.

It currently supports :
 - Magento
 - Magento2
 - Prestashop

It embeds following docker containers :
 - php-apache (with xhprof)
 - mysql
 - adminer (for mysql administration)
 - maildev (for mail sending)

## Pre-requisite

On host environment you should have installed :
 - [MANDATORY] docker https://docs.docker.com/install/linux/docker-ce/ubuntu/
 - [MANDATORY] docker-compose https://docs.docker.com/compose/install/#install-compose
 - [MANDATORY] envsubst
 - [MANDATORY] jq (sudo apt-get install jq) https://stedolan.github.io/jq/manual/
 - [OPTIONNAL] ngrok https://ngrok.com/

## Understand the folder structure

The folder structure given is the same for magento, prestashop or magento2, only the configuration files could be different, you should find :
 - `docker/*` the specific configuration for a given docker container
   - `docker/php_apache` with the php extension config required for the app
 - `shared/*` all files shared between host and docker container
   - `shared/app` should contain the app files here (for instance all magento or prestashop files)
   - `shared/conf` contains all the conf shared, for instance the php.ini file
   - `shared/data` contains the database
   - `shared/script` contains the scripts used by the init container for the initialization of the app

## Start Magento App

1. Copy `magento` folder where you want
2. Install Magento App in `shared/app` folder with plugins you want (maybe Oyst:))
3. Run `/bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT] && sudo docker-compose up`
for instance : `/bin/bash build-docker-compose.sh 8103 3307 8203 8303 && sudo docker-compose up`
4. Launch the default page of your Magento App to install it (The default page should be the ngrok url if you use it)

 - NB1 : The db credentials are : server:mysql, user:root, pwd:root
 - NB2 : Do not run build-docker-compose.sh as superuser
 - NB3 : If you use ngrok and each time you change base ngrok url, you should reuse the command `sudo /bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT]` before starting the docker container
 - NB4 : If you want to run something in the magento container you should run `sudo docker-compose exec php_apache /bin/bash`
 - NB5 : If you face an issue with base url : check in adminer the core_config_data table filtering by path = web/unsecure/base_url or path = web/secure/base_url and clean the cache

5. Assuming you used the previous ports `8103 3307 8203 8303` You should find the following functionnal urls :

 - [BASE_(NGROK)_URL] or http://127.0.0.1:8103 : Magento app
 - [BASE_(NGROK)_URL] or http://127.0.0.1:8103/xhprof_runs.php : xhprof Monitoring
 - http://127.0.0.1:8203 : adminer
 - http://127.0.0.1:8303 : maildev

## Start Prestashop App

1. Copy `prestashop` folder where you want
2. Install Prestashop App in `shared/app` folder with plugins you want (maybe Oyst:))
3. Run `/bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT] && sudo docker-compose up`
for instance : `/bin/bash build-docker-compose.sh 8102 3308 8202 8302 && sudo docker-compose up`
4. Launch the default page of your Prestashop App to install it (The default page should be the ngrok url if you use it)

 - NB1 : The db credentials are : server:mysql, user:root, pwd:root
 - NB2 : Do not run build-docker-compose.sh as superuser
 - NB3 : If you use ngrok and each time you change base ngrok url, you should reuse the command `sudo /bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT]` before starting the docker container
 - NB4 : If you want to run something in the prestashop container you should run `sudo docker-compose exec php_apache /bin/bash`
 - NB5 : If you face an issue with base url : check in adminer the shop_url table

5. Assuming you used the previous ports `8102 3308 8202 8302` You should find the following functionnal urls :

 - [BASE_(NGROK)_URL] or http://127.0.0.1:8102 : Prestashop app, 
   Notice that ngrok url does not work for prestashop, use http://127.0.0.1:8102/admin instead
 - [BASE_(NGROK)_URL] or http://127.0.0.1:8102/xhprof_runs.php : xhprof Monitoring
 - http://127.0.0.1:8202 : adminer
 - http://127.0.0.1:8302 : maildev

## Start Magento2 App

1. Copy `magento` folder where you want
2. Install Magento2 App in `shared/app` folder with plugins you want (maybe Oyst:) //TODO Plugin)
3. Run `/bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT] && sudo docker-compose up`
for instance : `/bin/bash build-docker-compose.sh 8104 3309 8204 8304 && sudo docker-compose up`
4. Launch the default page of your Magento2 App to install it (The default page should be the ngrok url if you use it)

 - NB1 : The db credentials are : server:mysql, user:root, pwd:root 
 - NB2 : Do not run build-docker-compose.sh as superuser
 - NB3 : If you use ngrok and each time you change base ngrok url, you should reuse the command `sudo /bin/bash build-docker-compose.sh [APP_PORT] [MYSQL_PORT] [ADMINER_PORT] [MAILDEV_PORT]` before starting the docker container
 - NB4 : If you want to run something in the magento2 container you should run `sudo docker-compose exec php_apache /bin/bash`
 - NB5 : If you face an issue with base url : check in adminer the core_config_data table filtering by path = web/unsecure/base_url or path = web/secure/base_url and clean the cache

5. Assuming you used the previous ports `8104 3309 8204 8304` You should find the following functionnal urls :

 - [BASE_(NGROK)_URL] or http://127.0.0.1:8104 : Magento2 app
 - [BASE_(NGROK)_URL] or http://127.0.0.1:8104/xhprof_runs.php : xhprof Monitoring
 - http://127.0.0.1:8204 : adminer
 - http://127.0.0.1:8304 : maildev

## Start Ahots Fan

Personal project based on php/mongodb stack, no more instructions here.

## Start Magento PWA Studio

1. Copy `magento_pwa_studio` folder where you want
2. Install Magento Venia Storefront in `shared/app` following : https://magento-research.github.io/pwa-studio/venia-pwa-concept/setup
3. Launch `/bin/bash build-docker-compose.sh 10000 && sudo docker-compose up`
