BASE_URL=$(curl -s http://localhost:4040/api/tunnels|jq -r '.tunnels[]|select((.config.addr|test("127.0.0.1:'$1'")) and .proto=="http")|.public_url') \
APP_PORT=$1 \
CURRENT_UID=$(id -u) \
CURRENT_GID=$(id -g) \
PORT_PREFIX='' \
envsubst < docker-compose.yml.template > docker-compose.yml
